class Reservation < ApplicationRecord
  belongs_to :client
  belongs_to :book
  belongs_to :librarian

  after_create :control_stock

  after_destroy :increase_stock

  def control_stock
    self.book.update(stock: book.stock-1)
  end


  def increase_stock
    self.book.update(stock: book.stock+1)
  end
end
