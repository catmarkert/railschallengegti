Rails.application.routes.draw do
  
  resources :reservations
  get 'home/index'
  root 'home#index'
  resources :librarians
  resources :clients
  resources :authors
  resources :books
  resources :categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
